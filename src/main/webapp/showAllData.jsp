<%@ page import="epam.task.servlets.model.PizzaItem" %>
<%@ page import="java.util.ArrayList" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<html>
<head><title>Show all</title></head>
<body>
<%
    ArrayList<PizzaItem> items = (ArrayList<PizzaItem>) request.getAttribute("posts");
    for (PizzaItem item: items) {
%>
<br>
<tr>
    <td><%=item.getName()%></td>
    <td><%=item.getStore()%></td>
    <td><%=item.getType()%></td>
    <td><%=item.getSize()%></td>
    <td><%=item.getCode()%></td>

</tr>
<%}%>

</body>
</html>
