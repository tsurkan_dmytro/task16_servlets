package epam.task.servlets;

import epam.task.servlets.db.DBMain;
import epam.task.servlets.model.PizzaItem;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;

@WebServlet("/stores")
public final class Storer extends HttpServlet {

    public static final String GREETING_REQUEST_PARAMETER_KEY = "greeting";
    private static final String NAME_REQUEST_PARAMETER_KEY = "name";
    private static final String SIZE_REQUEST_PARAMETER_KEY = "size";
    private static final String STORE_REQUEST_PARAMETER_KEY = "store";
    private static final String TYPE_REQUEST_PARAMETER_KEY = "type";
    private static final String CODE_REQUEST_PARAMETER_KEY = "code";

    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp)
            throws ServletException, IOException {

        handleRequestForGreeting(req, resp);
    }

    protected void doPost(final HttpServletRequest req, final HttpServletResponse resp)
            throws ServletException, IOException {
        PizzaItem pizzaItem = new PizzaItem();

        String paramName = req.getParameter("name");
        pizzaItem.setName((paramName == null || paramName.equals("")) ? "No data" : paramName );

        String paramSize = req.getParameter("size");
        int size = Integer.parseInt(paramSize);
        pizzaItem.setSize(size==0 ? 1 : size);

        String paramStore = req.getParameter("store");
        pizzaItem.setStore((paramStore == null || paramStore.equals("")) ? "No data" : paramStore );

        String paramType = req.getParameter("type");
        pizzaItem.setType((paramType == null || paramType.equals("")) ? "No data" : paramType );

        String paramCode = req.getParameter("code");
        int code = Integer.parseInt(paramCode);
        pizzaItem.setCode(code==0 ? 1 : code );

        DBMain.pizzaItems.add(pizzaItem);

        req.setAttribute("posts", DBMain.pizzaItems);
        req.getRequestDispatcher("/showAllData.jsp").forward(req, resp);
    }

    private void handleRequestForGreeting(final HttpServletRequest req, final HttpServletResponse resp)
            throws ServletException, IOException {
        assert !Objects.isNull(req) : "Request required for greeting request";
        assert !Objects.isNull(resp) : "Response required for greeting request";

        final String name = extractNameFromRequest(req);
        final String greeting = greet(name);

        req.setAttribute(GREETING_REQUEST_PARAMETER_KEY, greeting);
        req.getRequestDispatcher("/showAllData.jsp").forward(req, resp);
    }

    private String extractNameFromRequest(final HttpServletRequest req) {
        assert !Objects.isNull(req) : "Request required for name extraction";

        return req.getParameter(NAME_REQUEST_PARAMETER_KEY);
    }

    private String greet(final String name) {
        assert !Objects.isNull(name) && !name.isEmpty() : "Name required for greeting";

        return String.format("Hello %s, the date on the server is %s", name, "nvcmnb,cvn");
    }
}