package epam.task.servlets.model;

public class PizzaItem {

    public static int id = 1;

    private String store = "";
    private String type = "";
    private String name = "";
    private int size = 1;
    private int code = 1111;

    public PizzaItem() {
    }

    public static int getId() {
        return id;
    }

    public static void setId(int id) {
        PizzaItem.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getStore() {
        return store;
    }

    public void setStore(String store) {
        this.store = store;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
