package epam.task.servlets;

import epam.task.servlets.db.DBMain;
import epam.task.servlets.model.PizzaItem;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;

@WebServlet("/names")
public final class Name extends HttpServlet {

    public static final String GREETING_REQUEST_PARAMETER_KEY = "greeting";
    private static final String NAME_REQUEST_PARAMETER_KEY = "name";

    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp)
            throws ServletException, IOException {

    }

    protected void doPost(final HttpServletRequest req, final HttpServletResponse resp)
            throws ServletException, IOException {
        ArrayList<String> names = new ArrayList<>();

        for (PizzaItem item : DBMain.pizzaItems) {
            names.add(item.getName());
        }

        req.setAttribute("data", names);
        req.getRequestDispatcher("/showDetailData.jsp").forward(req, resp);
    }


}